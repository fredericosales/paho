#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Frederico Sales
<frederico.sales@engenharia.ufjf.br>
2018
"""

# imports
import paho.mqtt.client as mqtt
import time
import timeit
import logging
import pdb
import datetime


def warn(*args, **kwargs):
    pass


import warnings
warnings.warn = warn


# =========================================================================== #
# logging

"""
Logging levels

#1 - debug      (detailed info)
#2 - info       (confirmation that things according to plan 
#3 - warning    (something unexpected)
#4 - error      (some function failed application must close.)
#5 - critical	(application crashes on this)
"""

tstamp = datetime.date.today()
FORMAT = ''
LOG_FILENAME = 'mqtt-' + str(tstamp) + '.log'
logging.basicConfig(filename=LOG_FILENAME, level=logging.INFO, format=FORMAT)


# =========================================================================== #
# functions

def debug(debug=False):
    """Debug pdb trace."""

    if debug == True:
        pdb.set_trace()


def _time():
    """Medir o tempo"""
    begin = time.time()
    return begin


def main_mqtt(broker, port=1883, keepalive=60, cli_name='dummy_mqtt', topic="Home/Light", payload="On", QoS=0):
    """MQTT sending messages."""

    # set unique client name
    logging.info("Creating a new instance.")
    client = mqtt.Client(cli_name)

    # connect to broker
    logging.info("Connecting to broker: {}, port: {}".format(broker, port))
    client.connect(broker, port, keepalive)

    # client subscribe
    logging.info("Subscribing to topic: {}, payload: {}".format(topic, payload))
    client.subscribe(topic)

    # publishing messages
    logging.info("Publishing message to topic: {}".format(topic))
    client.publish(topic, payload, QoS)


def main_websockets(broker, port=9091, cli_name='dummy_websockets', topic="Home/Light", payload="On", QoS=0):
    """MQTT sending messages over websockets."""

    # set unique client name
    logging.info("Creating a new instance.")
    client = mqtt.Client(cli_name, transport='websockets')

    # connect to broker
    logging.info("Connecting to broker")
    client.connect(broker, port)

    # client subscribe
    logging.info("Subscribing to topic: {}".format(topic))
    client.subscribe(topic)

    # publishing messages
    logging.info("Publishing message to topic: {}".format(topic))
    client.publish(topic, payload, QoS)

    # client disconnect
    client.disconnect()


def testing():
    """Testing time measure."""
    for i in range(1, 150):
        time.sleep(0.15)


def dummy_mqtt(server="localhost"):
    """Dummy mqtt messages."""
    begin = _time()
    for i in range(1, 2001):
        if i % 2 == 0:
            main_mqtt(server, cli_name='NetLab', topic="PGCC/Lights/netlab", payload="On")
            par = _time()
            logging.info("ligado | {:.2f}".format(par - begin))
            logging.info("# ========================================================= #")

        else:
            main_mqtt(server, cli_name='NetLab', topic="PGCC/Lights/netlab", payload="Off")
            impar = _time()
            logging.info("desligado | {:.2f}".format(impar - begin))
            logging.info("# ========================================================= #")

    end = _time()
    logging.info("# Server: {} Total: {:.2f}".format(server, end - begin))
    logging.info("# ========================================================= #")


def dummy_websockets(server="localhost"):
    """Dummy websockets messages."""
    begin = _time()
    for i in range(1, 2001):
        if i % 2 == 0:
            main_websockets(server, cli_name='NetLab', topic="PGCC/Lights/netlab", payload="On")
            par = _time()
            logging.info("ligado | {:.2f}".format(par - begin))
            logging.info("# ========================================================= #")

        else:
            main_websockets(server, cli_name='NetLab', topic="PGCC/Lights/netlab", payload="Off")
            impar = _time()
            logging.info("desligado | {:.2f}".format(impar - begin))
            logging.info("# ========================================================= #")

    end = _time()
    logging.info("# Server: {} Total: {:.2f}".format(server, end - begin))
    logging.info("# ========================================================= #")


# =========================================================================== #
# main

if __name__ == "__main__":
    # do some:

    # # marking time
    # begin = _time()
    # testing()
    # end = _time()
    # print("Duracao: {:.2f}s".format(end - begin))

    # dummy
    dummy_mqtt()
    # dummy_mqtt("corexy")
    # dummy_websockets()
    # dummy_websockets("corexy")
    # dummy_mqtt('200.131.219.102')
    # dummy_websockets('200.131.219.102')
