#!/bin/zsh
#
# Frederico Sales
# <frederico.sales@engenharia.ufjf.br>
# 2018
#

esptool.py --port /dev/$1 --baud 460800 write_flash --flash_size=detect 0 $2